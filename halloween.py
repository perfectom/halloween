#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time
import pygame
import random


eyes_pin = 18
motion_pin = 4
servo_pin = 12

GPIO.setmode(GPIO.BCM)  
# Setup your channel
GPIO.setup(motion_pin, GPIO.IN) 
GPIO.setup(eyes_pin, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(servo_pin,GPIO.OUT)

Freq = 100
eyes_pwm = GPIO.PWM(eyes_pin, Freq)
dc=0                               # set dc variable to 0 for 0%
eyes_pwm.start(dc)                      # Start PWM with 0% duty cycle


p_control = [5,5.25,5.5,5.75,6,6.25,6.5,6.75,7,7.25,7.5,7.75,8,8.25,8.5,8.75,9,9.25,9.5,9.75,10]

p=GPIO.PWM(servo_pin,50)# 50hz frequency
p.start(7.5)# starting duty cycle ( it set the servo to 0 degree )


def scare_people():
    print("starting audio")
    
    pygame.init()
    pygame.mixer.init()
    pygame.mixer.music.load('evil_laugh.mp3')
    # GPIO.output(eyes_pin, GPIO.HIGH)
    pygame.mixer.music.play()


    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))

      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(0, 101, 10):    # Loop 0 to 100 stepping dc by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    for dc in range(95, 0, -10):    # Loop 95 to 5 stepping dc down by 5 each loop
      eyes_pwm.ChangeDutyCycle(dc)
      p.ChangeDutyCycle(random.choice(p_control))
      time.sleep(0.05)             # wait .05 seconds at current LED brightness
    #   print(dc)
    eyes_pwm.ChangeDutyCycle(0)
    p.ChangeDutyCycle(7.5)
    # while pygame.mixer.music.get_busy():
    #     # GPIO.output(eyes_pin, GPIO.HIGH)
    #     pygame.time.wait(1000)
    #     # for x in range(1,101):
    #     #     eyes_pwm.ChangeDutyCycle(x)
    #     #     time.sleep(.1)
    #     print("playing..")

    # GPIO.output(eyes_pin, GPIO.LOW)
 

# GPIO.setup(12, GPIO.OUT)  # Set GPIO pin 12 to output mode.
# pwm = GPIO.PWM(12, 100)   # Initialize PWM on pwmPin 100Hz frequency

# # main loop of program
# print("\nPress Ctl C to quit \n")  # Print blank line before and after message.
# dc=0                               # set dc variable to 0 for 0%
# pwm.start(dc)                      # Start PWM with 0% duty cycle

# try:
#   while True:                      # Loop until Ctl C is pressed to stop.
#     for dc in range(0, 101, 5):    # Loop 0 to 100 stepping dc by 5 each loop
#       pwm.ChangeDutyCycle(dc)
#       time.sleep(0.05)             # wait .05 seconds at current LED brightness
#       print(dc)
#     for dc in range(95, 0, -5):    # Loop 95 to 5 stepping dc down by 5 each loop
#       pwm.ChangeDutyCycle(dc)
#       time.sleep(0.05)             # wait .05 seconds at current LED brightness
#       print(dc)
# except KeyboardInterrupt:
#   print("Ctl C pressed - ending program")

# pwm.stop()                         # stop PWM




try:
    print("PIR Module Test (CTRL+C to exit)")
    time.sleep(2)
    print("Ready")
    while True:
        if GPIO.input(motion_pin):
            print("Motion Detected! Scaring in progress..")
            scare_people()
            time.sleep(.5)
        else:
            print("No motion, no scaring")
            time.sleep(.5)
except KeyboardInterrupt:
    print(" Quit")
    eyes_pwm.stop()
    GPIO.cleanup()

